LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 

 
ENTITY Test1 IS
END Test1;
 
ARCHITECTURE behavior OF Test1 IS 
 
    COMPONENT ArchivoDeRegistros
    PORT(
         read_register1 : IN  std_logic_vector(3 downto 0);
         read_register2 : IN  std_logic_vector(3 downto 0);
         write_register : IN  std_logic_vector(3 downto 0);
         shamt : IN  std_logic_vector(3 downto 0);
         write_data : IN  std_logic_vector(15 downto 0);
         SHE : IN  std_logic;
         DIR : IN  std_logic;
         WR : IN  std_logic;
         clk : IN  std_logic;
         clr : IN  std_logic;
         read_data1 : OUT  std_logic_vector(15 downto 0);
         read_data2 : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal read_register1 : std_logic_vector(3 downto 0) := (others => '0');
   signal read_register2 : std_logic_vector(3 downto 0) := (others => '0');
   signal write_register : std_logic_vector(3 downto 0) := (others => '0');
   signal shamt : std_logic_vector(3 downto 0) := (others => '0');
   signal write_data : std_logic_vector(15 downto 0) := (others => '0');
   signal SHE : std_logic := '0';
   signal DIR : std_logic := '0';
   signal WR : std_logic := '0';
   signal clk : std_logic := '0';
   signal clr : std_logic := '0';

 	--Outputs
   signal read_data1 : std_logic_vector(15 downto 0);
   signal read_data2 : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant clk_period : time := 100 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: ArchivoDeRegistros PORT MAP (
          read_register1 => read_register1,
          read_register2 => read_register2,
          write_register => write_register,
          shamt => shamt,
          write_data => write_data,
          SHE => SHE,
          DIR => DIR,
          WR => WR,
          clk => clk,
          clr => clr,
          read_data1 => read_data1,
          read_data2 => read_data2
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		


      wait;
   end process;

END;
