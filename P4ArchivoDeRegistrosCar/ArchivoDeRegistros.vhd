library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
 use ieee.std_logic_unsigned.all;

entity ArchivoDeRegistros is
    Port ( read_register1,read_register2 : in  STD_LOGIC_VECTOR (3 downto 0);
           write_register,shamt : in  STD_LOGIC_VECTOR (3 downto 0);
           write_data  : in  STD_LOGIC_VECTOR (15 downto 0);
           SHE,DIR,WR,clk,clr : in  STD_LOGIC;
           read_data1,read_data2 : out  STD_LOGIC_VECTOR (15 downto 0));
end ArchivoDeRegistros;

architecture Behavioral of ArchivoDeRegistros is
type arreglo is array(0 to 15) of STD_LOGIC_VECTOR(15 downto 0);
signal aux: arreglo;

begin
	process(clk,clr)
		begin
				if(clr='1') then --reset
					for i in 0 to 15 loop
					aux(i)<="0000000000000000";
					end loop;
				elsif(clk' event and clk='1')then
				if(WR='1') then
					if(SHE='0' and DIR= '0') then --escritura
						aux(conv_integer(write_register)) <= write_data;
					elsif(SHE='1' and DIR='0') then --corrimiento derecha
						aux(conv_integer(write_register))<=
						to_stdlogicvector((to_bitvector(aux(conv_integer(read_register1)))) SRL
						(conv_integer(shamt)));
					elsif(SHE='1' and DIR='1')then --corrimiento izquierda
						aux(conv_integer(write_register))<=
						to_stdlogicvector((to_bitvector(aux(conv_integer(read_register1)))) SLL
						(conv_integer(shamt)));
						end if;
					end if;
				end if;
			end process;
						
						--Lectura
						read_data1<=aux(conv_integer(read_register1));
						read_data2<=aux(conv_integer(read_register2));
end Behavioral;

